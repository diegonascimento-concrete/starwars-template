package br.com.viavarejo.starwars.feature.base

import android.support.v7.app.AppCompatActivity

open class BaseActivity : AppCompatActivity() {
}