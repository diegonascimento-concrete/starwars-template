package br.com.viavarejo.starwars.di

import android.arch.persistence.room.Room
import br.com.viavarejo.starwars.persistence.database.StarWarsDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.applicationContext


val databaseModule = applicationContext {
    bean("db") {
        Room.databaseBuilder(
                androidApplication(),
                StarWarsDatabase::class.java,
                StarWarsDatabase.DB_NAME).build()
    }

    bean { (get("db") as StarWarsDatabase).filmsDao() }
}

