/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.viavarejo.starwars.persistence.repository

import br.com.viavarejo.starwars.network.RequestResultCode
import br.com.viavarejo.starwars.util.LogStarWars
import com.google.gson.JsonSyntaxException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.net.ssl.SSLHandshakeException

abstract class BaseRepository {
    fun doRequest(execute: () -> Unit, failure: (resultCode: RequestResultCode) -> Unit) {
        try {
            execute()
        } catch (e: ConnectException) {
            LogStarWars.printError("Error: NO_CONNECTION_AVAILABLE", e)
            failure(RequestResultCode.NoConnectionAvailable())
        } catch (e: SocketTimeoutException) {
            LogStarWars.printError("Error: TIMEOUT", e)
            failure(RequestResultCode.Timeout())
        } catch (e: SSLHandshakeException) {
            LogStarWars.printError("Error: SSL_FAIL", e)
            failure(RequestResultCode.SslFail())
        } catch (e: UnknownHostException) {
            LogStarWars.printError("Error: UnknownHostException", e)
            failure(RequestResultCode.UnknownError())
        } catch (e: JsonSyntaxException) {
            LogStarWars.printError("Error: JSON_SYNTAX_RESULT", e)
            failure(RequestResultCode.JsonSyntaxResult())
        }
    }
}