/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.viavarejo.starwars.feature.films

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import br.com.viavarejo.starwars.model.ResultFilms
import br.com.viavarejo.starwars.network.RequestResultValue
import br.com.viavarejo.starwars.persistence.repository.FilmsRepository


class ListFilmsViewModel(var filmsRepository: FilmsRepository) : ViewModel() {
    private var mFilms: LiveData<RequestResultValue<ResultFilms>>? = null

    fun getFilms(): LiveData<RequestResultValue<ResultFilms>>? {
        if (mFilms == null) {
            mFilms = filmsRepository.getFilms()
        }

        return this.mFilms
    }

}