/*
 * Copyright (C) 2018 Diego Figueredo do Nascimento.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.viavarejo.starwars.persistence.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import br.com.viavarejo.starwars.model.ResultFilms
import br.com.viavarejo.starwars.network.RequestResultCode
import br.com.viavarejo.starwars.network.RequestResultValue
import br.com.viavarejo.starwars.network.starwars.StarWarsClient
import br.com.viavarejo.starwars.persistence.dao.FilmsDao
import java.util.concurrent.Executor


class FilmsRepository(private var starWarApi: StarWarsClient,
                      private var filmsDao: FilmsDao,
                      private var executor: Executor) : BaseRepository() {

    fun getFilms(): LiveData<RequestResultValue<ResultFilms>> {
        val data = MutableLiveData<RequestResultValue<ResultFilms>>()

        executor.execute {
            doRequest({
                var response = starWarApi.getApi().getFilms().execute()
                val requestResultCode = RequestResultCode.valueOf(response.code())
                if (response.isSuccessful) {
                    val result = RequestResultValue(response.body(), requestResultCode)

                    filmsDao.deleteFilms()
                    response.body()?.results?.forEach { filmsDao.insertFilm(it) }
                    result.value = response.body()
                    data.postValue(result)
                } else {
                    data.postValue(getFilmsDataBase(requestResultCode))
                }
            }, {
                data.postValue(getFilmsDataBase(it))
            })
        }

        return data
    }

    private fun getFilmsDataBase(resultCode: RequestResultCode): RequestResultValue<ResultFilms> {
        val resultFilms = ResultFilms()
        val result = RequestResultValue(resultFilms, resultCode)
        resultFilms.results = filmsDao.getFilms()
        return result
    }
}