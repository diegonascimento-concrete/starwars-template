package br.com.viavarejo.starwars.util

import android.util.Log
import br.com.viavarejo.starwars.BuildConfig

object LogStarWars {

    private val OPEN_BRACKET = "["
    private val CLOSE_BRACKET = "] - "
    private val FILE_EXTENSION = ".java"
    private val SEPARATOR = " - "
    private val METHOD_NAME_INDEX = 2
    private val DEFAULT_TAG = "starwars"
    private var DEBUG = BuildConfig.DEBUG //NOPMD - just a notation

    /***
     * Print the given message using a specific tag
     *
     * @param tag     the tag to be logged
     * @param message the message
     */
    fun print(tag: String, message: String) {
        if (DEBUG) {
            Log.i(tag, OPEN_BRACKET + getClassCaller() + CLOSE_BRACKET + message)
        }
    }

    /***
     * Print the error and the exception
     *
     * @param message the message about the error
     * @param e       the exception
     */
    fun printError(message: String, e: Throwable) {
        if (DEBUG) {
            Log.e(DEFAULT_TAG, OPEN_BRACKET + getClassCaller() + CLOSE_BRACKET + message, e)
        }
    }

    /**
     * Prints the given message including the name of the class and method that has invoked this function.
     */
    fun print(message: String) {
        if (DEBUG) {
            Log.i(DEFAULT_TAG, OPEN_BRACKET + getClassCaller() + CLOSE_BRACKET + message)
        }
    }

    private fun getClassCaller(): String {
        val t = Throwable()
        val elements = t.stackTrace
        return (elements[METHOD_NAME_INDEX].fileName.replace(FILE_EXTENSION, "")
                + SEPARATOR
                + elements[METHOD_NAME_INDEX].methodName)
    }

    /***
     * Active the application to log all messages
     *
     * @param isDebuggable true if active the log
     */
    fun setIsDebuggable(isDebuggable: Boolean) {
        DEBUG = isDebuggable
    }

}