package br.com.viavarejo.starwars.di

import br.com.viavarejo.starwars.network.starwars.StarWarsClient
import org.koin.dsl.module.applicationContext


val networkModule = applicationContext {
    bean { StarWarsClient }
}