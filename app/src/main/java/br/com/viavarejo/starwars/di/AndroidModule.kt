package br.com.viavarejo.starwars.di

import org.koin.dsl.module.applicationContext
import java.util.concurrent.Executor
import java.util.concurrent.Executors


val androidModule = applicationContext {
    bean("executor") { Executors.newSingleThreadExecutor() as Executor }
}