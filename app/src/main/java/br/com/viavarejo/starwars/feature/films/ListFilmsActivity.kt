package br.com.viavarejo.starwars.feature.films

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import br.com.viavarejo.starwars.R
import br.com.viavarejo.starwars.feature.base.BaseActivity
import br.com.viavarejo.starwars.feature.films.adapters.ItemFilmAdapter
import br.com.viavarejo.starwars.model.ResultFilms
import br.com.viavarejo.starwars.network.RequestResultCode
import br.com.viavarejo.starwars.network.RequestResultValue
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.architecture.ext.viewModel

class ListFilmsActivity : BaseActivity() {

    private val viewModel: ListFilmsViewModel by viewModel()
    private lateinit var adapter: ItemFilmAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
        subscribeUi(viewModel)
    }

    private fun initView() {
        val layoutManager = LinearLayoutManager(baseContext)
        lst_films.setHasFixedSize(false)
        lst_films.layoutManager = layoutManager
    }

    private fun subscribeUi(viewModel: ListFilmsViewModel) {
        // Update the list when the data changes
        viewModel.getFilms()?.observe(this, Observer<RequestResultValue<ResultFilms>> { films ->
            val list = films?.value?.results ?: listOf()
            adapter = ItemFilmAdapter(list)
            lst_films.adapter = adapter

            when (films?.requestResultCode) {
                !is RequestResultCode.Success -> {
                    Toast.makeText(baseContext, "Dados Off-line", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}
