package br.com.viavarejo.starwars.di

import br.com.viavarejo.starwars.feature.films.ListFilmsViewModel
import org.koin.android.architecture.ext.viewModel
import org.koin.dsl.module.applicationContext

val viewModelModule = applicationContext {
    viewModel {
        ListFilmsViewModel(get())
    }
}