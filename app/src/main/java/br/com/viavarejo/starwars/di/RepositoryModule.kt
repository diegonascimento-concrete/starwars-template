package br.com.viavarejo.starwars.di

import br.com.viavarejo.starwars.persistence.repository.FilmsRepository
import org.koin.dsl.module.applicationContext


val repositoryModule = applicationContext {
    bean { FilmsRepository(get(), get(), get("executor")) }
}
